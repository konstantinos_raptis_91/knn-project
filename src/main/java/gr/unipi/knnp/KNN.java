package gr.unipi.knnp;

import java.util.Arrays;
import java.util.Random;

class PointData {

    public double[] x;
    public double y;

    public PointData(double[] x, double y) {

        // count x columns
        int numberOfXColumns = x.length;

        // init field var x table
        this.x = new double[numberOfXColumns];

        // run through all that columns and copy the values 
        // from local var x table to field var x table
        for (int i = 0; i < x.length; i++) {
            this.x[i] = x[i];
        }

        // assign y value
        this.y = y;
    }

    public double dist(double[] x) {

        // throw an exception
        if (this.x.length != x.length) {
            throw new RuntimeException("Arrays should have the same length");
        }

        int numberOfColumns = this.x.length;
        double sum = 0;

        for (int i = 0; i < numberOfColumns; i++) {
            sum += Math.pow(this.x[i] - x[i], 2);
        }

        return Math.sqrt(sum);
    }

    @Override
    public String toString() {
        return "PointData{" + "x=" + Arrays.toString(x) + ", y=" + y + '}';
    }

}

class Node {

    public double score;
    public PointData ptd;
    public Node next;

    public Node(double[] x, double y) {
        ptd = new PointData(x, y);
    }

    @Override
    public String toString() {
        return "Node{" + "score=" + score + ", ptd=" + ptd + '}';
    }

}

class PointList {

    // the first node
    private Node first;
    // dim refer to x dimension
    private int dim;

    public PointList(int dim) {
        this.dim = dim;
        this.first = null;
    }

    public static PointList readData(String filename) {
        PointList list = null;
        java.io.BufferedReader br = null;
        try {
            br = new java.io.BufferedReader(new java.io.FileReader(filename));
            String line = br.readLine();
            String[] data = line.split(" ");
            int m = Integer.parseInt(data[0]);
            int n = Integer.parseInt(data[1]);
            list = new PointList(n);

            double[] x = new double[n];
            while ((line = br.readLine()) != null) {
                data = line.split(" ");
                for (int j = 0; j < n; j++) {
                    x[j] = Double.parseDouble(data[j]);
                }
                list.append(x, Double.parseDouble(data[n]));
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (java.io.IOException ex) {
                ex.printStackTrace();
            }
        }
        return (list);
    }

    public int getDim() {
        return dim;
    }

    public int length() {

        Node current = first;

        if (current == null) {
            return 0;
        }

        int numberOfNodes = 1;

        while (current.next != null) {
            current = current.next;
            numberOfNodes++;
        }

        return numberOfNodes;
    }

    public boolean append(double[] x, double y) {

        if (x.length != dim) {
            return false;
        }

        Node newNode = new Node(x, y);
        Node current = first;

        if (first == null) {
            first = newNode;
        } else {
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        }

        return true;
    }

    public boolean append(PointList list) {

        if (list.dim != this.dim) {
            return false;
        }

        Node current = list.first;

        while (current != null) {
            append(current.ptd.x, current.ptd.y);
            current = current.next;
        }

        return true;
    }

    public PointData rmFirst() {

        if (this.length() <= 0) {
            return null;
        }

        Node oldFirst = first;
        first = first.next;

        return oldFirst.ptd;
    }

    public void shuffle() {

        Random random = new Random();
        int currentLength = this.length();
        Node nodeToBeMoved;

        for (int i = 0; i < currentLength; i++) {
            int position = random.nextInt(this.length() - i);
            nodeToBeMoved = rm(position);
            this.append(nodeToBeMoved.ptd.x, nodeToBeMoved.ptd.y);
        }

    }

    public Node find(int position) {

        if (position > this.length() - 1) {
            return null;
        }

        Node current = first;
        int counter = 0;

        while (counter < position) {
            current = current.next;
            counter++;
        }

        return current;
    }

    public Node rm(int position) {

        if (position > this.length() - 1 || position < 0) {
            return null;
        }

        Node current = first;
        Node previous = current;
        int counter = 0;

        while (counter < position) {
            previous = current;
            current = current.next;
            counter++;
        }

        if (position == 0) {
            first = first.next;
        } else {
            previous.next = current.next;
        }

        return current;
    }

    public PointData rmNearest(double[] x) {

        Node current = first;
        PointData minDistPointData = null;
        
        if (current.next == null) {
            rm(0);
            minDistPointData = first.ptd;
        } else {
            double minDist = Double.MAX_VALUE;
            double currentDist;
            int minDistPosition = 0;
            int position = 0;
            
            while (current.next != null) {
                
                if ((currentDist = current.ptd.dist(x)) < minDist) {
                    minDistPosition = position;
                    minDist = currentDist;
                    minDistPointData = current.ptd;
                }
                position++;
                current = current.next;
            }
            
            rm(minDistPosition);
        }
        
        return minDistPointData;
    }

//    public PointList findKNearest(double[] x, int k) {
//    }
//
//    public double classify(double[] x) {
//    }
    
    public void display() {

        Node current = first;

        while (current != null) {
            System.out.println(current);
            current = current.next;
        }

    }

}

public class KNN {

    public static void main(String[] args) {

    }

}
