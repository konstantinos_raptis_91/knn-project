package gr.unipi.knnp;

import java.util.Arrays;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Konstantinos Raptis
 */
public class TestKNN {
    
    @Before
    public void setup() {
        
    }
    
    @Test
    public void testrmFirst() {
        PointList pointList = new PointList(2);
        
        double[] firstX = new double[]{35, 2};
        double firstY = 2;
        
        pointList.append(firstX, firstY);
        pointList.append(new double[]{50, 4}, 2);
        pointList.append(new double[]{65, 3}, 2);
        pointList.append(new double[]{90, 4}, 2);
        
        PointData data = pointList.rmFirst();
        
        Assert.assertArrayEquals(firstX, data.x, 0);
        Assert.assertEquals(firstY, data.y, 0);
        Assert.assertEquals(3, pointList.length());
    }
    
    @Test
    public void testrmFirstInSingleNodePointList() {
        PointList pointList = new PointList(2);
        
        double[] firstX = new double[]{35, 2};
        double firstY = 2;
        
        pointList.append(firstX, firstY);
        
        PointData data = pointList.rmFirst();
        
        Assert.assertArrayEquals(firstX, data.x, 0);
        Assert.assertEquals(firstY, data.y, 0);
        Assert.assertEquals(0, pointList.length());
    }
    
    @Test
    public void testrmFirstInEmptyPointList() {
        PointList pointList = new PointList(2);
        
        PointData data = pointList.rmFirst();
        
        Assert.assertEquals(null, data);
        Assert.assertEquals(0, pointList.length());
    }
    
    @Ignore
    @Test
    public void testFind() {
        
        PointList pointList = new PointList(2);

        pointList.append(new double[]{35, 2}, 2);
        pointList.append(new double[]{50, 4}, 2);
        pointList.append(new double[]{65, 3}, 2);
        pointList.append(new double[]{90, 4}, 2);
        
        pointList.display();
        pointList.rm(4);
        System.out.println();
        pointList.display();
    }
    
    @Test
    public void testShuffle() {
        
        PointList pointList = new PointList(2);
        
        pointList.append(new double[]{35, 2}, 2);
        pointList.append(new double[]{50, 4}, 2);
        pointList.append(new double[]{65, 3}, 2);
        pointList.append(new double[]{90, 4}, 2);
        pointList.append(new double[]{130, 5}, 2);
        pointList.append(new double[]{160, 4}, 2);
        pointList.append(new double[]{180, 6}, 2);
        pointList.append(new double[]{200, 5}, 2);
        
        pointList.display();
        pointList.shuffle();
        System.out.println();
        pointList.display();
    }
    
}
